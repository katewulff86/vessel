/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef VESSEL_PLUGIN_HPP
#define VESSEL_PLUGIN_HPP

#include <memory>

namespace Vessel {
    struct Plugin {
        Plugin(float sample_rate);
        ~Plugin();

        enum Port {
            OUTPUT,
        };

        void connect(Port, float *);
        void run(size_t);
    private:
        struct Impl;
        std::unique_ptr<Impl> pimpl;
    };
}

#endif

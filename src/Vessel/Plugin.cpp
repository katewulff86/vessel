/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <cmath>
#include <stdexcept>

#include "Plugin.hpp"

namespace Vessel {
    struct Plugin::Impl {
        Impl(float sample_rate)
            : sample_rate{sample_rate}
            , T{1.0f / sample_rate}
        {}

        float const sample_rate;
        float const T = 0.0;
        float *output_buffer = NULL;
        float phase = 0.0;
    };

    Plugin::Plugin(float sample_rate)
        : pimpl{std::make_unique<Impl>(sample_rate)}
    {}

    Plugin::~Plugin()
    {}

    void Plugin::connect(Port port, float *buffer) {
        switch (port) {
        case OUTPUT:
            pimpl->output_buffer = buffer;
            break;
        default:
            throw new std::invalid_argument("port not understood");
        };
    }

    void Plugin::run(size_t N) {
        for (size_t i = 0; i < N; ++i) {
            pimpl->output_buffer[i] =
                0.5 * sinf(100.0 * 2 * 3.141592654 * pimpl->phase);
            pimpl->phase += pimpl->T;
        }
    }
}


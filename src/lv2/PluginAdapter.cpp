/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <Vessel/Plugin.hpp>

#include "PluginAdapter.hpp"

enum Port {
    OUTPUT = 0,
};

struct PluginAdapter::Impl {
    Impl(double sample_rate)
        : plugin{std::make_unique<Vessel::Plugin>(sample_rate)}
    {}

    std::unique_ptr<Vessel::Plugin> plugin;
};

PluginAdapter::PluginAdapter(LV2_Descriptor const *descriptor,
                             double sample_rate,
                             char const *bundle_path,
                             LV2_Feature const *const *features)
    : pimpl{std::make_unique<Impl>(sample_rate)}
{
    (void) descriptor;
    (void) bundle_path;
    (void) features;
}

PluginAdapter::~PluginAdapter()
{}

void PluginAdapter::connect_port(uint32_t port, void *data) {
    switch ((Port) port) {
    case OUTPUT:
        pimpl->plugin->connect(Vessel::Plugin::Port::OUTPUT, (float *) data);
        break;
    default:
        break;
    }
}

void PluginAdapter::activate() {
}

void PluginAdapter::run(uint32_t N) {
    pimpl->plugin->run(N);
}

void PluginAdapter::deactivate() {
}


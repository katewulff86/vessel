/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "lv2/core/lv2.h"

#include "PluginAdapter.hpp"

#define VESSEL_URI "https://gitlab.com/katewulff86/vessel"

static LV2_Handle instantiate(const LV2_Descriptor *descriptor,
                              double sample_rate,
                              char const *bundle_path,
                              LV2_Feature const *const *features)
{
    PluginAdapter *plugin_adapter = new PluginAdapter(descriptor,
                                                      sample_rate,
                                                      bundle_path,
                                                      features);

    return (LV2_Handle) plugin_adapter;
}

static void connect_port(LV2_Handle instance, uint32_t port, void *data)
{
    PluginAdapter *plugin_adapter = reinterpret_cast<PluginAdapter *>(instance);
    plugin_adapter->connect_port(port, data);
}

static void activate(LV2_Handle instance)
{
    PluginAdapter *plugin_adapter = reinterpret_cast<PluginAdapter *>(instance);
    plugin_adapter->activate();
}

static void run(LV2_Handle instance, uint32_t N)
{
    PluginAdapter *plugin_adapter = reinterpret_cast<PluginAdapter *>(instance);
    plugin_adapter->run(N);
}

static void deactivate(LV2_Handle instance)
{
    PluginAdapter *plugin_adapter = reinterpret_cast<PluginAdapter *>(instance);
    plugin_adapter->deactivate();
}

static void cleanup(LV2_Handle instance)
{
    PluginAdapter *plugin_adapter = reinterpret_cast<PluginAdapter *>(instance);
    delete plugin_adapter;
}

static void const *extension_data(char const *uri)
{
    (void) uri;

    return NULL;
}

static LV2_Descriptor const descriptor = {
    VESSEL_URI,
    instantiate,
    connect_port,
    activate,
    run,
    deactivate,
    cleanup,
    extension_data
};

LV2_SYMBOL_EXPORT LV2_Descriptor const *lv2_descriptor(uint32_t index)
{
    return index == 0 ? &descriptor : NULL;
}


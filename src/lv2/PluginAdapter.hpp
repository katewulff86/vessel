/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef VESSEL_LV2_PLUGIN_ADAPTER_HPP
#define VESSEL_LV2_PLUGIN_ADAPTER_HPP

#include <memory>

#include "lv2/core/lv2.h"

struct PluginAdapter {
    PluginAdapter(LV2_Descriptor const *descriptor,
                  double sample_rate,
                  char const *bundle_path,
                  LV2_Feature const *const *features);
    ~PluginAdapter();

    void connect_port(uint32_t port, void *data);
    void activate();
    void run(uint32_t N);
    void deactivate();
private:
    struct Impl;
    std::unique_ptr<Impl> pimpl;
};

#endif

